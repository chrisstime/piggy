# Piggy

A money saving app.

- Rails 5.2.2
- Ruby 2.5.3

## Setup

Clone the Repo then install required gems and their dependencies
```
bundle install
```
Application can be found locally at `localhost:3000`. To start the server:
```
bin/rails server
```

