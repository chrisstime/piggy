module HaveLess
  class DrinksController < ApplicationController
    def index
      @drinks = Drink.all
    end

    def new
      @drink = Drink.new
    end

    def create
      @drink = Drink.new(drink_params)

      if @drink.save
        redirect_to have_less_drinks_path, notice: 'Drink created.'
      else
        render action: 'new'
      end
    end

    def edit; end

    def show
      @drink = Drink.find(params[:id])
    end

    private

    def drink_params
      params.require(:drink).permit(:name, :drink_type)
    end
  end
end
