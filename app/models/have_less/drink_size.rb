class DrinkSize < ApplicationRecord
  belongs_to :drink, dependent: :destroy

  enum sizes: %w[small medium large extra_large]
end
