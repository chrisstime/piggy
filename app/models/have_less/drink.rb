class Drink < ApplicationRecord
  has_many :drink_sizes

  validates_presence_of :name

  enum drink_type: %w[coffee milk_tea alcohol other]

  attr_accessor :name, :drink_type

  scope :coffee, -> { where(drink_type: :coffee) }
  scope :milk_tea, -> { where(drink_type: :milk_tea) }
  scope :alcohol, -> { where(drink_type: :alcohol) }
  scope :other, -> { where(drink_type: :other) }
end
