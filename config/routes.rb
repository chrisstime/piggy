Rails.application.routes.draw do
  namespace :have_less do
    resources :drinks
  end

  root 'welcome#index'
end
