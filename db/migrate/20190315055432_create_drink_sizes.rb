class CreateDrinkSizes < ActiveRecord::Migration[5.2]
  def change
    create_table :drink_sizes do |t|
      t.float :cost, null: false, default: 0.0
      t.integer :size, null: false
      t.references :drink, index: true, foreign_key: true

      t.timestamps
    end
  end
end
