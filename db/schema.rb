# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_03_15_055432) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "drink_sizes", force: :cascade do |t|
    t.float "cost", default: 0.0, null: false
    t.integer "size", null: false
    t.bigint "drink_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["drink_id"], name: "index_drink_sizes_on_drink_id"
  end

  create_table "drinks", force: :cascade do |t|
    t.string "name"
    t.integer "drink_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "drink_sizes", "drinks"
end
